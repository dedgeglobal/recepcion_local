<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@ include file="/html/portlet/login/init.jsp" %>
<style>
.portlet-topper{
	display:none!important;
}
.input-error{
	border: solid 1px #D0021B !important;
}
.gobmx{
	font-weight: 300 !important;
}
.gobmx .field {
	width: 100% !important;
	height: 34px !important;
	padding: 6px 12px !important;
	font-size: 14px !important;
	border-radius: 4px !important;
}
.required {
    font-size: 11px !important;
}
.equalTo {
    font-size: 11px !important;
}
.rangeLength {
    font-size: 11px !important;
}
</style>

<%
User user2 = (User)request.getAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_USER);

if (Validator.isNull(authType)) {
	authType = company.getAuthType();
}

Integer reminderAttempts = (Integer)portletSession.getAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_ATTEMPTS);

if (reminderAttempts == null) {
	reminderAttempts = 0;
}
%>

<portlet:renderURL portletMode="view" var="viewURL" />



 <!-- gobmx -->  
	<div class="gobmx">
		<div class="container">
		
		<div id="loading">
		
		<div id="hidePage" style="width: 3000px; height: 3000px; position: fixed; z-index: 100; background-color: rgba(192,192,192,0.3); margin: -300px;"></div>
 		<div id="contLoader" class="contLoader"><div class="loader"></div><div class="msgLoader">Espere por favor...</div></div>
 		
 		</div>
 		

		<div class="row col-md-12" id="msgInvalidUsername">
              <div class="alert alert-info">El n�mero de proveedor no se encuentra registrado.</div>
        </div>
        
		<div class="row col-md-12" id="msgBusquedaCorreo">
              <div class="alert alert-info">Verifique el correo electr�nico siguiente: <strong><span id="infomail"></span></strong>, ya que la nueva contrase�a ser� enviada a este correo, si el correo electr�nico no es correcto, favor de actualizarlo haciendo click en <a href="/desktop/cambiar-correo"> este enlace</a>.</div>
        </div>
            				
				<div class="row">
				          <div class="col-sm-12">
				        <!-- gobmx -->  
				          
				          
<portlet:actionURL var="forgotPasswordURL">
	<portlet:param name="struts_action" value="/login/forgot_password" />
</portlet:actionURL>

							

				              <h3 id="formulario">Recuperar Contrase�a</h3>
							  <hr class="red">
							  
<!-- 							  		  <div class="col-sm-8"> -->
							  		  
							  		  
<aui:form action="<%= forgotPasswordURL %>" method="post" name="fm" class="form-horizontal">
	<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />

	<portlet:renderURL var="redirectURL" />

	<aui:input name="redirect" type="hidden" value="<%= redirectURL %>" />

	<liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed" />

	<liferay-ui:error exception="<%= NoSuchUserException.class %>" message='<%= "the-" + TextFormatter.format(authType, TextFormatter.K) + "-you-requested-is-not-registered-in-our-database" %>' />
	<liferay-ui:error exception="<%= RequiredReminderQueryException.class %>" message="you-have-not-configured-a-reminder-query" />
	<liferay-ui:error exception="<%= SendPasswordException.class %>" message="your-password-can-only-be-sent-to-an-external-email-address" />
	<liferay-ui:error exception="<%= UserActiveException.class %>" message="your-account-is-not-active" />
	<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="please-enter-a-valid-email-address" />
	<liferay-ui:error exception="<%= UserLockoutException.class %>" message="this-account-has-been-locked" />
	<liferay-ui:error exception="<%= UserReminderQueryException.class %>" message="your-answer-does-not-match-what-is-in-our-database" />

	<aui:fieldset>
		<c:choose>
			<c:when test="<%= user2 == null %>">

				<%
				String loginParameter = null;
				String loginLabel = null;

				if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
					loginParameter = "emailAddress";
					loginLabel = "email-address";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
					loginParameter = "screenName";
					loginLabel = "screen-name";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
					loginParameter = "userId";
					loginLabel = "id";
				}

				String loginValue = ParamUtil.getString(request, loginParameter);
				%>

				<aui:input name="step" type="hidden" value="1" />


				
				<div class="row">
				 <div class="form-group">
					
					<div class="col-sm-6">
						<aui:input id="screenName" label="N�mero de proveedor" name="<%= loginParameter %>" size="30" type="text" value="<%= loginValue %>">
							<aui:validator name="required" />
						</aui:input>
					</div>
					<div class="col-sm-6">&nbsp;</div>
				</div>
				</div>
				<div class="row">
				<div class="form-group">
     				<div class="col-sm-6">
     					<div id="infomail"></div>
     				</div>
    			</div>
    			</div>
				<div class="row">
				<div class="form-group">
					<div class="col-sm-6">
						<c:if test="<%= PropsValues.CAPTCHA_CHECK_PORTAL_SEND_PASSWORD %>">
							<portlet:resourceURL var="captchaURL">
								<portlet:param name="struts_action" value="/login/captcha" />
							</portlet:resourceURL>
		
							<liferay-ui:captcha url="<%= captchaURL %>" />
						</c:if>
					</div>
					<div class="col-sm-6">&nbsp;</div>
				</div>
				</div>
				<div class="row">
				<div class="form-group">
					
					<div class="col-sm-6">
						<aui:button-row class="pull-right">
							<aui:button cssClass="btn btn-primary pull-right" type="submit" value='<%= PropsValues.USERS_REMINDER_QUERIES_ENABLED ? "next" : "send-new-password" %>' />
						&nbsp;	
							<aui:button cssClass="btn btn-default pull-right" type="cancel" value='Cancelar' onClick='<%= viewURL %>'/>
						</aui:button-row>
					</div>
					<div class="col-sm-6">
						&nbsp;
					</div>
				</div>
				</div>



			</c:when>
			<c:when test="<%= (user2 != null) && Validator.isNotNull(user2.getEmailAddress()) %>">
				<aui:input name="step" type="hidden" value="2" />
				<aui:input name="emailAddress" type="hidden" value="<%= user2.getEmailAddress() %>" />

				<c:if test="<%= Validator.isNotNull(user2.getReminderQueryQuestion()) && Validator.isNotNull(user2.getReminderQueryAnswer()) %>">

					<%
					String login = null;

					if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
						login = user2.getEmailAddress();
					}
					else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
						login = user2.getScreenName();
					}
					else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
						login = String.valueOf(user2.getUserId());
					}
					%>

					<div class="alert alert-info">
						<%= LanguageUtil.format(pageContext, "a-new-password-will-be-sent-to-x-if-you-can-correctly-answer-the-following-question", login) %>
					</div>

					<aui:input autoFocus="<%= true %>" label="<%= HtmlUtil.escape(LanguageUtil.get(pageContext, user2.getReminderQueryQuestion())) %>" name="answer" type="text" />
				</c:if>

				<c:choose>
					<c:when test="<%= PropsValues.USERS_REMINDER_QUERIES_REQUIRED && !user2.hasReminderQuery() %>">
						<div class="alert alert-info">
							<liferay-ui:message key="the-password-cannot-be-reset-because-you-have-not-configured-a-reminder-query" />
						</div>
					</c:when>
					<c:otherwise>
						<c:if test="<%= reminderAttempts >= 3 %>">
							<portlet:resourceURL var="captchaURL">
								<portlet:param name="struts_action" value="/login/captcha" />
							</portlet:resourceURL>

							<liferay-ui:captcha url="<%= captchaURL %>" />
						</c:if>

						<aui:button-row>
						 	
							<aui:button type="submit" value='<%= company.isSendPasswordResetLink() ? "send-password-reset-link" : "send-new-password" %>'/>
						</aui:button-row>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<div class="alert alert-block">
					<liferay-ui:message key="the-system-cannot-send-you-a-new-password-because-you-have-not-provided-an-email-address" />
				</div>
			</c:otherwise>
		</c:choose>
	</aui:fieldset>
</aui:form>



<!-- gobmx -->
<!-- </div> -->

</div>
</div>
</div>
</div>
<!-- gobmx -->


<liferay-util:include page="/html/portlet/login/navigation.jsp" />

<script type="text/javascript">

/*
function msieversion() {

	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/")){ //test for MSIE x.x;
		return true;
	}
	return false;
}
	

function showHideDiv(strDiv)
{
    var divstyle = document.getElementById(strDiv).style.visibility;
    if(divstyle.toLowerCase() == "visible" || divstyle == "")
    {
        document.getElementById(strDiv).style.visibility = "hidden";

    
    }
    else
    {
        document.getElementById(strDiv).style.visibility = "visible";
    }
}

*/
$(document).ready(function() {	
	$("#msgBusquedaCorreo").hide();
	$("#msgInvalidUsername").hide();

	$('#_58_screenName').blur(function(){
		var val = $( this ).val();
   


		$('#loading').show();

	
   
		Liferay.Service(
     
				'/UserInfo-portlet.cuser/get-user-by-screen-name',
     
				{
       
					screenName: val
     
				},
     
				function(obj) {
       
					console.log(obj);

					

					
					

      
					if((obj != "java.lang.NullPointerException") && (obj.length > 0)) {
      
						console.log("se cumple la condicion");
      
						$("#infomail").text(obj);
      
						$("#msgBusquedaCorreo").show();
						$('#loading').hide();
						$("#msgInvalidUsername").hide();    


					}
     
					else {
						
						if((obj == "java.lang.NullPointerException")){
							
							$("#msgInvalidUsername").show();
							$("#msgBusquedaCorreo").hide();    
							$('#loading').hide();

						}else{

							$("#msgInvalidUsername").hide();
							$("#msgBusquedaCorreo").hide();    
							$('#loading').hide();
							
						}
						   
						console.log("No se cumple la condicion");

					}
    
				}
				
		);


	});
 
	$('#loading').hide();

});
</script>