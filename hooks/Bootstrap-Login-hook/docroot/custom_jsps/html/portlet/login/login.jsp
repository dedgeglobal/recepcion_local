<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@ include file="/html/portlet/login/init.jsp" %>
<style>
.portlet-topper{
display:none!important;
}
.input-error{
	border: solid 1px #D0021B !important;
}
.gobmx{
	font-weight: 300 !important;
}
</style>


<%
long companyId = themeDisplay.getCompanyId();
//local
long companyIdParam = 52906L;
//local
//long companyIdParam = 10154L;
//Local Armando
// long companyIdParam = 21302L
//desarrollo
//long companyIdParam = 23103L;
//QA Nuevo de la 58
// long companyIdParam = 20474L;
//Produccion
// long companyIdParam = 20351L;
%>


<c:choose>
	<c:when test="<%= themeDisplay.isSignedIn() %>">
	
	<div class="gobmx">
		<div class="container">
				<div class="row">
				          <div class="col-sm-12">

		<%
		String signedInAs = HtmlUtil.escape(user.getFullName());

		if (themeDisplay.isShowMyAccountIcon() && (themeDisplay.getURLMyAccount() != null)) {
			String myAccountURL = String.valueOf(themeDisplay.getURLMyAccount());

			if (PropsValues.DOCKBAR_ADMINISTRATIVE_LINKS_SHOW_IN_POP_UP) {
				signedInAs = "<a class=\"signed-in\" href=\"javascript:Liferay.Util.openWindow({dialog: {destroyOnHide: true}, title: '" + HtmlUtil.escapeJS(LanguageUtil.get(pageContext, "my-account")) + "', uri: '" + HtmlUtil.escapeJS(myAccountURL) + "'});\">" + signedInAs + "</a>";
			}
			else {
				myAccountURL = HttpUtil.setParameter(myAccountURL, "controlPanelCategory", PortletCategoryKeys.MY);

				signedInAs = "<a class=\"signed-in\" href=\"" + HtmlUtil.escape(myAccountURL) + "\">" + signedInAs + "</a>";
			}
		}
		%>

		<%= LanguageUtil.format(pageContext, "you-are-signed-in-as-x", signedInAs, false) %>
		
		</div></div></div>></div>
	</c:when>
	<c:otherwise>
	
		<div class="gobmx">
		<div class="container">
				<div class="row">
				          <div class="col-sm-12">
	
	 	<%
		String redirect = ParamUtil.getString(request, "redirect");
		String login = LoginUtil.getLogin(request, "login", company);
		String password = StringPool.BLANK;
		boolean rememberMe = ParamUtil.getBoolean(request, "rememberMe");

		if (Validator.isNull(authType)) {
			authType = company.getAuthType();
		}
		%>
		
		<portlet:actionURL secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="loginURL">
			<portlet:param name="struts_action" value="/login/login" />
		</portlet:actionURL>
	
			
		<% if(companyId == companyIdParam){ %>
		
		
			<liferay-ui:error exception="<%= AuthException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-login-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
			<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
			<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserLockoutException.class %>" message="this-account-has-been-locked" />
			<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="authentication-failed" />   
			
			
			
				              <h3 id="formulario">Iniciar Sesi�n</h3>
							  <hr class="red">
							  
							  		  <div class="col-sm-8">
						              <aui:form action="<%= loginURL %>" autocomplete='<%= PropsValues.COMPANY_SECURITY_LOGIN_FORM_AUTOCOMPLETE ? "on" : "off" %>' cssClass="form-horizontal" method="post" id="fm" name="fm">
						              
						              
											<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
											<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
											<aui:input name="doActionAfterLogin" type="hidden" value="<%= portletName.equals(PortletKeys.FAST_LOGIN) ? true : false %>" />
											
												<c:choose>
													<c:when test='<%= SessionMessages.contains(request, "userAdded") %>'>
									
														<%
														String userEmailAddress = (String)SessionMessages.get(request, "userAdded");
														String userPassword = (String)SessionMessages.get(request, "userAddedPassword");
														%>
									
														<div class="alert alert-success">
															<c:choose>
																<c:when test="<%= company.isStrangersVerify() || Validator.isNull(userPassword) %>">
																	<%= LanguageUtil.get(pageContext, "thank-you-for-creating-an-account") %>
									
																	<c:if test="<%= company.isStrangersVerify() %>">
																		<%= LanguageUtil.format(pageContext, "your-email-verification-code-has-been-sent-to-x", userEmailAddress) %>
																	</c:if>
																</c:when>
																<c:otherwise>
																	<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-your-password-is-x", userPassword, false) %>
																</c:otherwise>
															</c:choose>
									
															<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_ENABLED) %>">
																<%= LanguageUtil.format(pageContext, "your-password-has-been-sent-to-x", userEmailAddress) %>
															</c:if>
														</div>
													</c:when>
													<c:when test='<%= SessionMessages.contains(request, "userPending") %>'>
									
														<%
														String userEmailAddress = (String)SessionMessages.get(request, "userPending");
														%>
									
														<div class="alert alert-success">
															<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-you-will-be-notified-via-email-at-x-when-your-account-has-been-approved", userEmailAddress) %>
														</div>
													</c:when>
												</c:choose>
			
					
				
							                <div class="form-group">
							                  <label class="col-sm-offset-1 col-sm-4 control-label" for="email-03" style="text-align: left !important;"><span style="font-weight: bold; font-size: 18px;">N�mero de Proveedor<span id="astProvBasic">*</span><span id="astProvImportant" style="color: #D0021B;">*</span>:</span></label>

							                  
							                  <div class="col-sm-7">
							                    <input class="form-control" id="<portlet:namespace/>login" name="<portlet:namespace/>login" placeholder="Ingrese su n�mero de proveedor" type="text" value="<%= login %>">
							                    <span style="color:#D0021B; font-size:11px;" ng-show="<portlet:namespace/>login.$dirty && <portlet:namespace/>login.$invalid">
                                          			<span id="provRequ" ng-show="<portlet:namespace/>login.$error.required">El campo es requerido</span>
                                            	</span>
							                  </div>
							                </div>
							                <div class="form-group">
							                  <label class="col-sm-offset-1 col-sm-4 control-label" for="password" style="text-align: left !important; font-size: 18px;">Contrase�a<span id="astPassBasic">*</span><span id="astPassImportant" style="color: #D0021B;">*</span>:</label>
							                  <div class="col-sm-7">
							                    <input class="form-control" id="<portlet:namespace/>password"  name="<portlet:namespace/>password" placeholder="Contrase�a" type="password" value="<%= password %>">
							                  	<span style="color:#D0021B; font-size:11px;" ng-show="<portlet:namespace/>password.$dirty && <portlet:namespace/>password.$invalid">
                                          			<span id="passRequ" ng-show="<portlet:namespace/>password.$error.required">El campo es requerido</span>
                                            	</span>
							                  </div>
							                </div>
							                <div class="form-group">
							                  <div class="col-sm-offset-5 col-sm-7">
							                  
							                  	<div class="col-sm-6">
													<c:if test="<%= company.isAutoLogin() && !PropsValues.SESSION_DISABLED %>">
														<aui:input checked="<%= rememberMe %>" name="rememberMe" type="checkbox" />
													</c:if>
							                  	</div>
							                  	
							                   	<div class="col-sm-6">
							                   	&nbsp;
							                   	<!-- 
								                  <a class="pull-right" style="font-size:13px; margin-right: -15px;" href="?p_p_id=58&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=4&_58_struts_action=%2Flogin%2Fforgot_password">Recuperar Contrase�a</a>
							                  	 -->
							                  	</div>
							                  
							                  

							                  </div>
							                  
							                  
							                </div>
							                <div class="form-group">
							                  <div class="col-sm-offset-1 col-sm-3 control-label" style="text-align: left !important;">
  												<label style="color: #545454; font-weight: unset !important;">* Campos obligatorios</label>
  											  </div>
							                  <div class="col-sm-8">
							                    <button class="btn btn-primary pull-right" type="submit" value="sign-in">Acceder</button>
							                  </div>
							                </div>
						              </aui:form>	
						              
           							  
           							  </div>
				          
		<%}else{%>
		
		

		<aui:form action="<%= loginURL %>" autocomplete='<%= PropsValues.COMPANY_SECURITY_LOGIN_FORM_AUTOCOMPLETE ? "on" : "off" %>' cssClass="sign-in-form" method="post" name="fm">
			<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
			<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
			<aui:input name="doActionAfterLogin" type="hidden" value="<%= portletName.equals(PortletKeys.FAST_LOGIN) ? true : false %>" />

			<c:choose>
				<c:when test='<%= SessionMessages.contains(request, "userAdded") %>'>

					<%
					String userEmailAddress = (String)SessionMessages.get(request, "userAdded");
					String userPassword = (String)SessionMessages.get(request, "userAddedPassword");
					%>

					<div class="alert alert-success">
						<c:choose>
							<c:when test="<%= company.isStrangersVerify() || Validator.isNull(userPassword) %>">
								<%= LanguageUtil.get(pageContext, "thank-you-for-creating-an-account") %>

								<c:if test="<%= company.isStrangersVerify() %>">
									<%= LanguageUtil.format(pageContext, "your-email-verification-code-has-been-sent-to-x", userEmailAddress) %>
								</c:if>
							</c:when>
							<c:otherwise>
								<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-your-password-is-x", userPassword, false) %>
							</c:otherwise>
						</c:choose>

						<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_ENABLED) %>">
							<%= LanguageUtil.format(pageContext, "your-password-has-been-sent-to-x", userEmailAddress) %>
						</c:if>
					</div>
				</c:when>
				<c:when test='<%= SessionMessages.contains(request, "userPending") %>'>

					<%
					String userEmailAddress = (String)SessionMessages.get(request, "userPending");
					%>

					<div class="alert alert-success">
						<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-you-will-be-notified-via-email-at-x-when-your-account-has-been-approved", userEmailAddress) %>
					</div>
				</c:when>
			</c:choose>

			<liferay-ui:error exception="<%= AuthException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-login-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
			<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
			<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserLockoutException.class %>" message="this-account-has-been-locked" />
			<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="authentication-failed" />

			<aui:fieldset>

				<%
				String loginLabel = null;

				if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
					loginLabel = "email-address";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
					loginLabel = "screen-name";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
					loginLabel = "id";
				}
				%>

				<aui:input autoFocus="<%= windowState.equals(LiferayWindowState.EXCLUSIVE) || windowState.equals(WindowState.MAXIMIZED) %>" cssClass="clearable" label="<%= loginLabel %>" name="login" showRequiredLabel="<%= false %>" type="text" value="<%= login %>">
					<aui:validator name="required" />
				</aui:input>

				<aui:input name="password" showRequiredLabel="<%= false %>" type="password" value="<%= password %>">
					<aui:validator name="required" />
				</aui:input>

				<span id="<portlet:namespace />passwordCapsLockSpan" style="display: none;"><liferay-ui:message key="caps-lock-is-on" /></span>

				<c:if test="<%= company.isAutoLogin() && !PropsValues.SESSION_DISABLED %>">
					<aui:input checked="<%= rememberMe %>" name="rememberMe" type="checkbox" />
				</c:if>
			</aui:fieldset>

			<aui:button-row>
				<aui:button type="submit" value="sign-in" />
			</aui:button-row>
		</aui:form>

		<liferay-util:include page="/html/portlet/login/navigation.jsp" />

		<aui:script use="aui-base">
			var password = A.one('#<portlet:namespace />password');

			if (password) {
				password.on(
					'keypress',
					function(event) {
						Liferay.Util.showCapsLock(event, '<portlet:namespace />passwordCapsLockSpan');
					}
				);
			}
		</aui:script>
		
		
		<%}%>
		
		</div>
				</div>
				
				<script type="text/javascript">
	if($(".alert-error").is(":visible")) {
		$("#astPassImportant").show();
		$("#astProvImportant").show();
		$("#astPassBasic").hide();
		$("#astProvBasic").hide();
		$(".form-control").addClass("input-error");
		$(".form-control").addClass("input-error");
		$(".alert-error")[0].remove();
		$(".alert-error").addClass("alert-danger");
	} else {
		$("#astPassImportant").hide();
		$("#astProvImportant").hide();
		$("#astPassBasic").show();
		$("#astProvBasic").show();
		$(".form-control").removeClass("input-error");
		$(".form-control").removeClass("input-error");
	}
	
	
	


</script>
				
			</div>
		</div>	 		
			 
	</c:otherwise>
</c:choose>
*****************