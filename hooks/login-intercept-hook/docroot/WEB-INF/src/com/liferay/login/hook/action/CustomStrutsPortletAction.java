package com.liferay.login.hook.action;

import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.kernel.struts.BaseStrutsPortletAction;
import com.liferay.portal.kernel.struts.StrutsPortletAction;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.AutoLogin;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

import java.util.Enumeration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CustomStrutsPortletAction extends BaseStrutsPortletAction {

	
	
	
	
	
	   public void processAction(
	            StrutsPortletAction originalStrutsPortletAction,
	            PortletConfig portletConfig, ActionRequest actionRequest,
	            ActionResponse actionResponse)
	        throws Exception {
		   
	        ThemeDisplay themeDisplay =
	            (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	
	        /*
	        ClassLoader cl = PortalClassLoaderUtil.getClassLoader();
	        Class loginUtil=cl.loadClass("com.liferay.portlet.login.util.LoginUtil");
	        
	        java.lang.reflect.Method loginC =loginUtil.getDeclaredMethod("login", new Class[] { javax.servlet.http.HttpServletRequest.class, javax.servlet.http.HttpServletResponse.class, String.class, String.class, boolean.class, String.class } );
*/


	        /*
	        Long currentuser = themeDisplay.getUserId();

	        if (currentuser != null) {
	            System.out.println("Custom Struts Action 2");

	        }*/
		   
		   
		           
		   
		   //System.out.println("login:::: :) " + actionRequest.getParameter("login"));
		   
	       
		   
		  // actionRequest.setAttribute("login", "test@imss.com");
		   //actionResponse.setRenderParameter("login", "test@imss.com");
		   
		   
		   
		   
		   //actionResponse.setProperty("login", "test@imss.com");
		   
		   
		   
		   /*
			String login = ParamUtil.getString(actionRequest, "login");
			String password = actionRequest.getParameter("password");
			boolean rememberMe = ParamUtil.getBoolean(actionRequest, "rememberMe");
			String authType = ParamUtil.getString(actionRequest, "authType");
	
		   
		   



			if (Validator.isNotNull(login) && Validator.isNotNull(password)) {
				//LoginUtil.login(actionRequest, actionRequest, login, password, rememberMe, authType);

				
				loginC.invoke( null, new Object[] { actionRequest, actionRequest, login, password, true, com.liferay.portal.model.CompanyConstants.AUTH_TYPE_EA } );

			}
			
		   
		   System.out.println("login: " + login);
		   System.out.println("password: " + password);
		   
		   
		   */
		   
		   
	        originalStrutsPortletAction.processAction(
	            originalStrutsPortletAction, portletConfig, actionRequest,
	            actionResponse);
	    }

	    public String render(
	            StrutsPortletAction originalStrutsPortletAction,
	            PortletConfig portletConfig, RenderRequest renderRequest,
	            RenderResponse renderResponse)
	        throws Exception {

	        System.out.println("Custom Struts Action");

	        return originalStrutsPortletAction.render(
	            null, portletConfig, renderRequest, renderResponse);

	    }

	    public void serveResource(
	            StrutsPortletAction originalStrutsPortletAction,
	            PortletConfig portletConfig, ResourceRequest resourceRequest,
	            ResourceResponse resourceResponse)
	        throws Exception {

	        originalStrutsPortletAction.serveResource(
	            originalStrutsPortletAction, portletConfig, resourceRequest,
	            resourceResponse);

	    }

	
}