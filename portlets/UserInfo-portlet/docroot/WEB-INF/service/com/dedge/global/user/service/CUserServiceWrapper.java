/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.dedge.global.user.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CUserService}.
 *
 * @author alex
 * @see CUserService
 * @generated
 */
public class CUserServiceWrapper implements CUserService,
	ServiceWrapper<CUserService> {
	public CUserServiceWrapper(CUserService cUserService) {
		_cUserService = cUserService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _cUserService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_cUserService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _cUserService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public java.lang.String getUserByScreenName(java.lang.String screenName) {
		return _cUserService.getUserByScreenName(screenName);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CUserService getWrappedCUserService() {
		return _cUserService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCUserService(CUserService cUserService) {
		_cUserService = cUserService;
	}

	@Override
	public CUserService getWrappedService() {
		return _cUserService;
	}

	@Override
	public void setWrappedService(CUserService cUserService) {
		_cUserService = cUserService;
	}

	private CUserService _cUserService;
}