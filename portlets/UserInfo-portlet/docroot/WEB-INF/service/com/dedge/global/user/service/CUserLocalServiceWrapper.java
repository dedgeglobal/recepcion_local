/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.dedge.global.user.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CUserLocalService}.
 *
 * @author alex
 * @see CUserLocalService
 * @generated
 */
public class CUserLocalServiceWrapper implements CUserLocalService,
	ServiceWrapper<CUserLocalService> {
	public CUserLocalServiceWrapper(CUserLocalService cUserLocalService) {
		_cUserLocalService = cUserLocalService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _cUserLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_cUserLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _cUserLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.model.User getUserByScreenName(
		java.lang.String screenName) {
		return _cUserLocalService.getUserByScreenName(screenName);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CUserLocalService getWrappedCUserLocalService() {
		return _cUserLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCUserLocalService(CUserLocalService cUserLocalService) {
		_cUserLocalService = cUserLocalService;
	}

	@Override
	public CUserLocalService getWrappedService() {
		return _cUserLocalService;
	}

	@Override
	public void setWrappedService(CUserLocalService cUserLocalService) {
		_cUserLocalService = cUserLocalService;
	}

	private CUserLocalService _cUserLocalService;
}