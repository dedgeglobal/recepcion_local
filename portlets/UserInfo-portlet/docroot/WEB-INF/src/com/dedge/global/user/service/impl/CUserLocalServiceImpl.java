/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.dedge.global.user.service.impl;

import com.dedge.global.user.service.base.CUserLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

/**
 * The implementation of the c user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.dedge.global.user.service.CUserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author alex
 * @see com.dedge.global.user.service.base.CUserLocalServiceBaseImpl
 * @see com.dedge.global.user.service.CUserLocalServiceUtil
 */
public class CUserLocalServiceImpl extends CUserLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.dedge.global.user.service.CUserLocalServiceUtil} to access the c user local service.
	 */
	public static String VIRTUAL_HOST = PortletProps.get("com.dedge.company.virtualhost");
	
	
	
	public User getUserByScreenName(String screenName){
		User tpmUser =  null;
		
		Company company;
		try {
			company = CompanyLocalServiceUtil.getCompanyByVirtualHost(VIRTUAL_HOST);
			
			if(Validator.isNotNull(screenName)){
				
				User user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(), screenName);
				
				tpmUser = UserLocalServiceUtil.createUser(user.getUserId());
				tpmUser.setEmailAddress(user.getEmailAddress());
				
				System.out.println("email address:::: " + user.getEmailAddress());
				
			}
			
			
		} catch (PortalException e) {
			System.out.println("PortalException");
		} catch (SystemException e) {
			System.out.println("SystemException");
		}

		

		
		
		return tpmUser;
	}
}