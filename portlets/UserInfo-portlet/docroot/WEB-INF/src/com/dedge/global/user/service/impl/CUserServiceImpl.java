/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.dedge.global.user.service.impl;

import com.dedge.global.user.service.CUserLocalServiceUtil;
import com.dedge.global.user.service.base.CUserServiceBaseImpl;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.EmailAddress;
import com.liferay.portal.model.User;
import com.liferay.portal.security.ac.AccessControlled;

/**
 * The implementation of the c user remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.dedge.global.user.service.CUserService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author alex
 * @see com.dedge.global.user.service.base.CUserServiceBaseImpl
 * @see com.dedge.global.user.service.CUserServiceUtil
 */
public class CUserServiceImpl extends CUserServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.dedge.global.user.service.CUserServiceUtil} to access the c user remote service.
	 */
	
	@AccessControlled(guestAccessEnabled=true)
	public String getUserByScreenName(String screenName){
		
		if(Validator.isNotNull(screenName)){
			
			 return  CUserLocalServiceUtil.getUserByScreenName(screenName).getEmailAddress();
			
		}
		
		return null;
	}
}