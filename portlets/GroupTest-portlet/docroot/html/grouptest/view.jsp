<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@page import="com.liferay.portal.model.Group" %>
<%@page import="com.liferay.portal.service.GroupLocalServiceUtil" %>
<%@page import="java.util.List" %>


<portlet:defineObjects />


<%



long[] groupIds = {32209,30367,33227,30961,29727,34286};

List<Group> groups =  GroupLocalServiceUtil.getGroups(groupIds);




///GroupLocalServiceUtil.updateGroup(group)

for(Group group:groups){

%>
<h4>ID: <%=group.getGroupId()%> - FriendlyURL: <%=group.getFriendlyURL()%> - DescriptiveName: <%=group.getDescriptiveName()%> Type - <%=group.getTypeLabel() %> - <%=group.getTreePath() %> </h4>
<%
List<Group> ancestors = group.getAncestors();

for(Group ancestor:ancestors){
	%>
	
	<h6>ID: <%=ancestor.getGroupId()%> - FriendlyURL: <%=ancestor.getFriendlyURL()%> - DescriptiveName: <%=ancestor.getDescriptiveName()%> Type - <%=ancestor.getTypeLabel() %> - <%=ancestor.getTreePath() %> </h6>
	
	
	<%
}

}
%>