package com.liferay.docs.mailbox.portlet;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

/**
 * Portlet implementation class MailBoxPortlet
 */
public class MailBoxPortlet extends MVCPortlet {
 
	private static String VIRTUAL_HOST = PortletProps.get("com.dedge.company.virtualhost");
	private static Company company;
	private User user;

	
	
	public void enviarCorreo(ActionRequest request,  ActionResponse  response){
		
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);


		String username =  uploadPortletRequest.getParameter("username");
		String nombre =  uploadPortletRequest.getParameter("nombre");
		String rfc =  uploadPortletRequest.getParameter("rfc");
		String email =  uploadPortletRequest.getParameter("email");
		String mensaje =  uploadPortletRequest.getParameter("mensaje");
		File archivo = uploadPortletRequest.getFile("archivo");

		
		
		
		
		
		try {
		
			//company = CompanyLocalServiceUtil.getCompanyByVirtualHost(VIRTUAL_HOST);
			company = CompanyLocalServiceUtil.getCompany(themeDisplay.getCompanyId());
			
			
			//String isValid = simple_validate(username, rfc, email, nombre , mensaje, archivo);
			//valida
			//if(Validator.isBlank(isValid)){
				
				
				//if(Validator.isNotNull(user)){
					
					String subject = "Buzón de sugerencias y generación de tickets: #" + username;
					InternetAddress to =  null;
					InternetAddress from =  null;
					
					
					
					try {
						to = new InternetAddress("soporte.imss@pispdigital.freshdesk.com");
						//from = new InternetAddress("soporte.imss@bovedadigital.net");

						
						if((Validator.isNotNull(email)) && (!Validator.isBlank(email)) && (Validator.isEmailAddress(email))){
							from = new InternetAddress(email);
						}
						
						
						MailMessage mailMessage = new MailMessage(from, to, subject, makeMailPage(username, rfc, email, nombre, mensaje), true);

					
						
						
						if(archivo.isFile()){
							mailMessage.addFileAttachment(archivo);
						}
						
						
						
						MailServiceUtil.sendEmail(mailMessage);			

						String urlpaths = "/desktop/";
						try {
							response.sendRedirect(urlpaths);
						} catch (IOException e) {
						}
						
					} catch (AddressException e) {
					
					}
					
					
					
					
					
					//envia correo
					System.out.println("nombre: " + nombre);
					System.out.println("rfc: " + rfc);
					System.out.println("email: " + email);
					System.out.println("username: " + username);
					System.out.println("mensaje: " + mensaje);
					
					
					System.out.println("Envia correo...");
					
				//}
				
				
				
			/*}else{
				

				System.out.println("error!!!!");
				
				
				SessionMessages.add(request, 
						PortalUtil.getPortletId(request) 
						+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				
				
				SessionErrors.add(request, isValid);
				
				
			}*/
		
			
		} catch (PortalException e) {
		
		} catch (SystemException e) {
		
		}
			
	
		

		
	
	}	
	


	private String simple_validate(String username, String rfc, String email, String nombre, String mensaje, File archivo){
		
		

		/*
		try{
			//error general en username
			
			if((Validator.isBlank(username)) || (Validator.isNull(username)) || (Validator.isHTML(username)) || (username.length() > 10)){
				return "error-username";
			}else{
				
				
				Criterion criterion = PropertyFactoryUtil.forName("screenName").eq(username);
				criterion = RestrictionsFactoryUtil.and(criterion , RestrictionsFactoryUtil.eq("companyId", company.getCompanyId()));

				
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(User.class).add(criterion);
				
					//error en username, no existe
					if(UserLocalServiceUtil.dynamicQuery(dynamicQuery).isEmpty()){
						return "error-username-not-exists";
					}else{
						user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(), username);
						
						//error en username, se encuentra inactivo
						if(user.getStatus() != 0){
							return "error-inactive-username";
						}	
					}
			}
	
			
			//error general en rfc
			if((Validator.isBlank(rfc)) || (Validator.isNull(rfc)) ||(!Validator.isAlphanumericName(rfc)) || (rfc.length() > 20)){
				return "error-rfc";
			}
			
			
			//error general en email
			if((Validator.isBlank(email)) || (!Validator.isEmailAddress(email))){
				return "error-email";
			}

			
			//error general en nombre
			if((Validator.isBlank(nombre)) || (Validator.isHTML(nombre)) || (!Validator.isName(nombre))){
				return "error-nombre";
			}
		
			
			//error general en mensaje
			if((Validator.isBlank(mensaje)) || (Validator.isHTML(mensaje)) || (mensaje.length() > 1024)){
				return "error-mensaje";
			}
			
			
			
			
			

			if(archivo.isFile()){
			
				if(!Validator.isFileExtension(".pdf")){
					
					return "error-archivo";
					
				}
				
			}
			
			
			
			
		
		}catch(SystemException e){
		}catch (PortalException e) {
		}
		
		
		*/
				
		return "";
	}
	
	
	
	public String makeMailPage(String username, String rfc, String email, String nombre, String mensaje)
    {
		
		
        String bodyHtml = "Nombre: " +  nombre + "<br>";
        	   bodyHtml += "RFC: " +  rfc.toUpperCase() + "<br>";
        	   bodyHtml += "Correo Electronico: " +  email + "<br>";
        	   bodyHtml += "Número de Proveedor: " +  username + "<br>";
        	   bodyHtml += "Mensaje: </br>" +  mensaje + "<br>";

        
         return bodyHtml;
	}
	
	
}
