<%@ include file="init.jsp" %>
<style>
.gobmx .input-error{
	border: solid 1px #D0021B !important;
}
</style>

<portlet:actionURL name="enviarCorreo" var="enviarCorreoURL"></portlet:actionURL>	

<div class="gobmx">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

	        <liferay-ui:error key="error-username" message="Verifique el N�mero de Proveedor."/>
	            <liferay-ui:error key="error-email" message="Verifique el Correo Electr�nico."/>
	              <liferay-ui:error key="error-username-not-exists" message="El N�mero de Proveedor no existe."/>
	                <liferay-ui:error key="error-inactive-username" message="El N�mero de Proveedor no se encuentra activo."/>
	                  <liferay-ui:error key="error-rfc" message="Verifique el RFC."/>
	                    <liferay-ui:error key="error-nombre" message="Verifique el nombre."/>
	                      <liferay-ui:error key="error-mensaje" message="Verifique el mensaje."/>
                      	   	<liferay-ui:error key="error-archivo" message="Error en el archivo."/>
                      
           
		</div>
	</div>
</div>



<a data-toggle="modal" href="#<portlet:namespace />quejassugerencias" role="button" style="display:none;">Soporte Funcional y Reporte de Hallazgos.</a></div>
			




<div aria-labelledby="myModalLabel" class="modal fade" id="<portlet:namespace />quejassugerencias" role="dialog" tabindex="-1000">
	<div class="modal-dialog modal-lg" role="dialog">
		<div class="modal-content">
			<div class="gobmx">
			
				<div class="modal-header">
					<h4 class="modal-title">Soporte Funcional y Reporte de Hallazgos.</h4>
				</div>

				<div class="modal-body">
				
					<div class="alert alert-info" style="display: block !important; text-align:justify !important;">
                 		Con la finalidad de mejorar nuestra calidad de servicio, ponemos a su disposici�n el siguiente buz�n para que nos haga llegar requerimientos e incidentes relacionados con el servicio de Recepci�n de Comprobantes Fiscales.
						Gracias por su tiempo.
                	</div>
                
                
					<form action="<%=enviarCorreoURL%>"  method="post"  id="<portlet:namespace />enviarCorreoForm" name="<portlet:namespace />enviarCorreoForm" enctype="multipart/form-data">								
              
                		<div class="form-group">
                  			<label for="<portlet:namespace />nombre">Nombre<span class="campo-Requerido1">*</span>:</label>
                  			<input class="form-control" type="text"  id="<portlet:namespace />nombre" name="<portlet:namespace />nombre" placeholder="Ingrese su nombre"/>
                			<span class="msgError" id="msgNomRequ">El campo es requerido</span>
                		</div>
                
                		<div class="form-group">
                  			<label for="<portlet:namespace />rfc">RFC<span class="campo-Requerido2">*</span>:</label>
                  			<input class="form-control" type="text"  id="<portlet:namespace />rfc" name="<portlet:namespace />rfc" placeholder="Ingrese su rfc" style="text-transform:uppercase"/>
                  			<span class="msgError" id="msgRfcRequ">El campo es requerido</span>
                			<span class="msgError" id="msgRfcInva">Ingrese un RFC correcto</span>
                		</div>
                
                		<div class="form-group">
                  			<label for="<portlet:namespace />email">Correo electr�nico<span class="campo-Requerido3">*</span>:</label>
                  			<input class="form-control" type="text"  id="<portlet:namespace />email" name="<portlet:namespace />email" placeholder="Ingrese su email"/>
                			<span class="msgError" id="msgMailRequ">El campo es requerido</span>
                			<span class="msgError" id="msgMailInva">Ingrese un email correcto</span>
                		</div>
                
                		<div class="form-group">
                  			<label for="<portlet:namespace />username">N�mero de Proveedor<span class="campo-Requerido4">*</span>:</label>
                  			<input class="form-control" type="text"  id="<portlet:namespace />username" name="<portlet:namespace />username" placeholder="Ingrese su N�mero de proveedor"/>
                			<span class="msgError" id="msgProvRequ">El campo es requerido</span>
                		</div>
                		
                		<div class="form-group">
    						<label class="control-label" for="<portlet:namespace />archivo">Cargar archivo:</label>
    						<input id="<portlet:namespace />archivo"  name="<portlet:namespace />archivo" type="file">
  						</div>
                                                
                		<div class="form-group">
                  			<label for="<portlet:namespace />mensaje">Mensaje<span class="campo-Requerido5">*</span>:</label>
                  			<textarea class="form-control" id="<portlet:namespace />mensaje" name="<portlet:namespace />mensaje" rows="3"></textarea>
                  			<span class="msgError" id="msgMjeRequ">El campo es requerido</span>
                		</div>
                
                		<button id="btnEnviarTicket" type="submit" class="btn btn-primary pull-right disabled">Enviar</button>
                		<label style="width: 5px !important; height: 1px !important;" class="pull-right">&nbsp;</label>
                		<button id="btnCerrarForm" class="btn btn-danger pull-right" data-dismiss="modal" type="button">Cerrar</button>
                		<label style="color: #545454; font-weight: 300 !important; padding-top:10px!important;">* Campos obligatorios</label>
                		<br>
                	
              		</form>
				</div>
			</div>
		</div>
	</div>
</div>





<script>
	var exprCorreo = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
	var exprRFC = /^([a-zA-Z,�,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[a-zA-Z0-9|\d]{3})$/;
	var nombrValido = false;
	var rfcValido = false;
	var eMailValido = false;
	var proveValido = false;
	var msajeValido = false;
	
	$(document).ready(function(){
		$(".msgError").fadeOut();
	});
	
	$("#<portlet:namespace />nombre").bind('blur keyup',
			function validaNombre(){
				nombrValido = false;
				if($(this).val().length < 1) {  
			        $("#msgNomRequ").fadeIn("slow"); //Reemplaza al metodo show()
			        $("#<portlet:namespace />nombre").addClass("input-error");
			        $(".campo-Requerido1").addClass("danger");
			    } else {
			    	$("#msgNomRequ").fadeOut(); //Reemplaza al metodo hide()
			    	$("#<portlet:namespace />nombre").removeClass("input-error");
			    	$(".campo-Requerido1").removeClass("danger");
			    	nombrValido = true;
			    }
		    	activarBtnEnviarTicket();
			}
	);
	
	$("#<portlet:namespace />rfc").bind('blur keyup',
			function validaRFC(){
				rfcValido = false;
				if($(this).val().length < 1) {
					$("#msgRfcRequ").fadeIn("slow"); //Reemplaza al metodo show()
					$("#<portlet:namespace />rfc").addClass("input-error");
					$(".campo-Requerido2").addClass("danger");
				} else if(!exprRFC.test($(this).val())) {  
			        $("#msgRfcInva").fadeIn("slow"); //Reemplaza al metodo show()
			        $("#msgRfcRequ").fadeOut(); //Reemplaza al metodo hide()
			        $("#<portlet:namespace />rfc").addClass("input-error");
			        $(".campo-Requerido2").addClass("danger");
			    } else {
			    	$("#msgRfcInva").fadeOut(); //Reemplaza al metodo hide()
			    	$("#msgRfcRequ").fadeOut(); //Reemplaza al metodo hide()
			    	$("#<portlet:namespace />rfc").removeClass("input-error");
			    	$(".campo-Requerido2").removeClass("danger");
			    	rfcValido = true;
			    }
				activarBtnEnviarTicket();
			}
	);
	
	$("#<portlet:namespace />email").bind('blur keyup',
	function validaEmail(){
		eMailValido = false;
		if($(this).val().length < 1) {
			$("#msgMailRequ").fadeIn("slow"); //Reemplaza al metodo show()
			$("#<portlet:namespace />email").addClass("input-error");
			$(".campo-Requerido3").addClass("danger");
		} else if(!exprCorreo.test($(this).val())) {  
	        $("#msgMailInva").fadeIn("slow"); //Reemplaza al metodo show()
	        $("#msgMailRequ").fadeOut(); //Reemplaza al metodo hide()
	        $("#<portlet:namespace />email").addClass("input-error");
	        $(".campo-Requerido3").addClass("danger");
	    } else {
	    	correo1 = $(this).val();
	    	$("#msgMailInva").fadeOut(); //Reemplaza al metodo hide()
	    	$("#msgMailRequ").fadeOut(); //Reemplaza al metodo hide()
	    	$("#<portlet:namespace />email").removeClass("input-error");
	    	$(".campo-Requerido3").removeClass("danger");
	    	eMailValido = true;
	    }
		activarBtnEnviarTicket();
	}
	);
	
	$("#<portlet:namespace />username").bind('blur keyup',
	function validaProveedor(){
		proveValido = false;
		if($(this).val().length < 1) {  
	        $("#msgProvRequ").fadeIn("slow"); //Reemplaza al metodo show()
	        $("#<portlet:namespace />username").addClass("input-error");
	        $(".campo-Requerido4").addClass("danger");
	    } else {
	    	$("#msgProvRequ").fadeOut(); //Reemplaza al metodo hide()
	    	$("#<portlet:namespace />username").removeClass("input-error");
	    	$(".campo-Requerido4").removeClass("danger");
	    	proveValido = true;
	    }
		activarBtnEnviarTicket();
	}
	);
	
	$("#<portlet:namespace />mensaje").bind('blur keyup',
			function validaProveedor(){
				msajeValido = false;
				if($(this).val().length < 1) {  
			        $("#msgMjeRequ").fadeIn("slow"); //Reemplaza al metodo show()
			        $("#<portlet:namespace />mensaje").addClass("input-error");
			        $(".campo-Requerido5").addClass("danger");
			    } else {
			    	$("#msgMjeRequ").fadeOut(); //Reemplaza al metodo hide()
			    	$("#<portlet:namespace />mensaje").removeClass("input-error");
			    	$(".campo-Requerido5").removeClass("danger");
			    	msajeValido = true;
			    }
				activarBtnEnviarTicket();
			}
	);
	
	$("#btnCerrarForm").mouseup(
		function limpiarFormulario() {
			$("#<portlet:namespace />enviarCorreoForm")[0].reset();
		}
	);
	
	
	function activarBtnEnviarTicket() {
		if(nombrValido && rfcValido && eMailValido && proveValido && msajeValido) {
			$("#btnEnviarTicket").removeClass('disabled');
			$("#btnEnviarTicket").attr({'disabled' : false});
		} else {
			$("#btnEnviarTicket").addClass('disabled');
			$("#btnEnviarTicket").attr({'disabled' : true});
		}
	}
	
</script>