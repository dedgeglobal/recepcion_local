<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>


<style>
.gobmx{
	font-weight: 300 !important;
}

form.aui .text[disabled] {
	background-color: #f5f5f5;
	color: #999;
}

form.aui .field-group, form.aui .group {
	box-sizing: border-box;
	clear: both;
	padding: 4px 0 4px 145px;
	position: relative;
	margin: 1px 0;
	width: 100%;
}

form.aui .field-group>aui-label, form.aui .field-group>label {
	float: left;
	margin-left: -145px;
	padding: 5px 0 0 0;
	position: relative;
	text-align: right;
	width: 130px;
	word-wrap: break-word;
}

.gobmx .field {
	width: 100% !important;
	height: 34px !important;
	padding: 6px 12px !important;
	font-size: 14px !important;
	border-radius: 4px !important;
}
.required {
    font-size: 11px !important;
}
.equalTo {
    font-size: 11px !important;
}
.rangeLength {
    font-size: 11px !important;
}
</style>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<!-- <script src="/escritorio-virtual-theme/js/jquery-1.11.1.min.js"></script> -->

<div class="gobmx">
	<div class="container">

		<span id="msg-block"></span>
		
<!-- 		<div class="alert alert-danger" id="alertaRequeridos"><strong>�Datos incompletos!</strong> no ha llenado uno o varios campos requeridos. Por favor verifique.</div> -->

		<div class="row col-sd-12">
			<div class="pull-right">
				<table class="table table-bordered">
					<tr>
						<td><span class="ng-binding">Raz&oacute;n Social: <span
								id="idTitleRazonSoc"></span> &nbsp;&nbsp;&nbsp;<span
								id="salirId"><a ng-click="salir()">Salir</a></span></td>
					</tr>
					<tr id="rfcIdTr">
						<td><span class="ng-binding">RFC: <span
								id="idTitleRFC"></span></td>
					</tr>
					<tr>
						<td><span class="ng-binding">No. Proveedor: <span
								id="idTitleNumProv"></span></span></td>
					</tr>
				</table>
			</div>
		</div>
	
		<div class="col-sd-12">
		<h3>Cambiar Contrase&ntilde;a</h3>
		<hr class="red"></hr>
		</div>

		<aui:form name="fm" id="fm" action="" method="post" class="form-horizontal">
	
			<div class="row">
            	<div class="form-group">
                	<label class="col-sm-3 control-label" style="font-weight: bold; font-size:17px;" for="<portlet:namespace/>usuario">Usuario:</label>
                	<div class="col-sm-3">
                		<input class="form-control" type="text" value="<%= user.getScreenName() %>" id="<portlet:namespace/>usuario" name="<portlet:namespace/>usuario" title="Usuario" disabled>
            		</div>
            	</div>
            </div>
            <br>
           
           <div class="row">
            	<div class="form-group">
                	<label class="col-sm-3 control-label" style="font-weight: bold; font-size:17px;" for="<portlet:namespace/>password1" accesskey="p">Nueva Contrase�a:</label>
                	<div class="col-sm-3">
                		<aui:input id="password1" inputCssClass="form-control" placeholder="Contrase�a" name="password1" showRequiredLabel="false" type="password" label=""></aui:input>
                	</div>
            	</div>
            </div>
            <br>
            
            <div class="row">
            	<div class="form-group">
                	<label class="col-sm-3 control-label" style="font-weight: bold; font-size:17px;" for="<portlet:namespace/>password2" accesskey="p">Confirmar Nueva Contrase�a:</label>
                	<div class="col-sm-3">
                		<aui:input id="password2" inputCssClass="form-control" placeholder="Confirmar contrase�a" name="password2" showRequiredLabel="false" type="password" label=""></aui:input>  
                	</div>              
            	</div>
            </div>
            <br>
            
            <div class="row">
            	<div class="form-group">
            		<div class="col-sm-offset-3 col-sm-3">
            			<aui:button id="update-password" value="Actualizar" cssClass="btn btn-primary pull-right"></aui:button>
            		</div>
            		<div class="col-sm-6">&nbsp;</div>
            	</div>
            </div>
            <br>
	
		</aui:form>
		

		
	</div>
</div>
<aui:script use="node, event, aui-form-validator, aui-alert">


	var password1Node =  A.one('#<portlet:namespace />password1');
	var password2Node =  A.one('#<portlet:namespace />password2');
	
	var msgBlock=A.one('#msg-block');


		var rules = {
			<portlet:namespace/>password1 : {
				required : true,
				rangeLength: [5,20]
			},

			<portlet:namespace/>password2 : {
				equalTo : '#<portlet:namespace/>password1',
				required : true
			}
		};

		
		var fieldStrings = {
			<portlet:namespace/>password1 : {
				required : 'Este campo no puede estar vac�o.'
			},

			<portlet:namespace/>password2 : {
				required : 'Este campo no puede estar vac�o.'
			}
		};

		var validator = new A.FormValidator({
			boundingBox : '#<portlet:namespace/>fm',
			fieldStrings : fieldStrings,
			rules : rules,
			showAllMessages : false
		});

		var updatePasswordButton = A.one('#update-password');

		updatePasswordButton.on('click', function(event) {
			msgBlock.empty();
		    var password1 = password1Node.get("value");
		    var password2 = password2Node.get("value");
		    
			validator.validate();

			if (!validator.hasErrors()) {
				Liferay.Service(
						  '/user/update-password',
						  {
						    userId:Liferay.ThemeDisplay.getUserId(),
						    password1: password1,
						    password2: password2,
						    passwordReset: false
						  },
						  function(obj) {
						    
						    if(obj === "com.liferay.portal.UserPasswordException"){
						    	 var errorMessageNode = A.Node.create('<br><div class="portlet-msg-error alert-danger">La contrase�a no puede ser igual a la actual.</div>');                               
						    	 	 errorMessageNode.appendTo(msgBlock);
						    			
						    }
						    else{
						    	 var successMessageNode = A.Node.create('<br><div class="portlet-msg-success">La contrase�a fue actualizada.</div>');
	    	 						successMessageNode.appendTo(msgBlock);	    	 					
						    }
						  }
				);
								
				console.log("no errors");
			}

		});	
		
// 		if((".form-validator-stack").is(":visible")) {
// 			alert("Clase visible");
// 			$("#alertaRequeridos").show();
// 		} else {
// 			alert("Clase invisible");
// 			$("#alertaRequeridos").hide();
// 		}
// 		if($(".alert-error").is(":visible")) {
// 			$(".alert-error").addClass("alert-danger");
// 		}
</aui:script>

