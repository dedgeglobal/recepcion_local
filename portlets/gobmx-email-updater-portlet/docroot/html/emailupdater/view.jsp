<%@ include file="init.jsp" %>

<portlet:actionURL name="cambiarContrasena" var="cambiarContrasenaURL"></portlet:actionURL>	


<div class="gobmx">


    <div class="container">
    <liferay-ui:error key="error-email-updater" message="Ocurri� un Error."/>
        <liferay-ui:error key="error-username" message="Verifique el N�mero de Proveedor."/>
            <liferay-ui:error key="error-email" message="Verifique el Correo Electr�nico."/>
                <liferay-ui:error key="error-email-exists" message="El Correo Electr�nico ya se encuentra registrado."/>
    	            <liferay-ui:error key="error-inactive-username" message="El N�mero de Proveedor no se encuentra activo."/>
                    	<liferay-ui:error key="error-username-not-exists" message="El N�mero de Proveedor no se encuentra registrado en este portal."/>
                        	<liferay-ui:error key="error-rfc" message="El RFC es inv�lido"/>
    
    
    <liferay-ui:error key="error-datos-incorrectos" message="Alguno de los datos proporcionados son Incorrectos"/>
    
    




        <div class="row">
            <div class="col-sd-12"> 
                <h2>Actualizar Correo</h2> <hr class="red"> 
            </div>
        </div>
        <div class="row">
            <div class="col-sd-6">
                <div class="form-group"> 



      			<form action="<%=cambiarContrasenaURL%>"  method="post"  id="<portlet:namespace />addForm" name="<portlet:namespace />addForm" enctype="multipart/form-data">								

                <label for="<portlet:namespace />username" class="col-md-9">N�mero de proveedor:</label>                 
                <div class="col-md-9"> 
                    <input type="text" id="<portlet:namespace />username"  class="form-control" placeholder="proveedor" name="<portlet:namespace />username">
                </div>


				
                <label for="<portlet:namespace />rfc" class="col-md-9">RFC:</label>                 
                <div class="col-md-9"> 
                    <input type="text" id="<portlet:namespace />rfc"  class="form-control" placeholder="RFC" name="<portlet:namespace />rfc">
                </div>
 				

                <label for="<portlet:namespace />email1" class="col-md-9">Correo Electr�nico:</label>                 
                <div class="col-md-9"> 
                    <input type="text" id="<portlet:namespace />email1"  class="form-control" placeholder="email@ejemplo.com" name="<portlet:namespace />email1">
                </div>


                <label for="<portlet:namespace />email2" class="col-md-9">Confirma Correo Electr�nico:</label>                 
                <div class="col-md-9"> 
                    <input type="text" id="<portlet:namespace />email2"  class="form-control" placeholder="email@ejemplo.com" name="<portlet:namespace />email2">
                </div>


				
				   
                
                <div class="form-group">
                    <div class="col-md-9" align="center">                        

						<aui:button-row class="pull-right">
							<aui:button cssClass="btn btn-primary pull-right" type="submit" value='Actualizar Correo' />
								<div class="pull-right" style="width:5px;">&nbsp;</div> 
							<aui:button cssClass="btn btn-default pull-right" type="cancel" value='Cancelar' onClick="location.href = '/desktop/inicio'"/>
						</aui:button-row>

                    </div> 
                </div>

                </div>
                 </form>
            </div>
        </div>
     
    </div>
</div>