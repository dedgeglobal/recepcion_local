package com.test;

import com.dedge.global.custom.ConectorBaseDatos;
import com.dedge.global.custom.CorrelacionadorWSClient;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class EmailUpdaterPortlet
 */
public class EmailUpdaterPortlet extends MVCPortlet {
	
	private static String VIRTUAL_HOST = PortletProps.get("com.dedge.company.virtualhost");
	private static Company company;
	private User user;
	
	
	public void cambiarContrasena(ActionRequest request,  ActionResponse  response){
	
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		
		String username =  uploadPortletRequest.getParameter("username");
		String rfc =  uploadPortletRequest.getParameter("rfc");
		String email1 =  uploadPortletRequest.getParameter("email1");
		String email2 =  uploadPortletRequest.getParameter("email2");
		
		

		
		try {

			
			company = CompanyLocalServiceUtil.getCompanyByVirtualHost(VIRTUAL_HOST);		


 
			String isValid = simple_validate(username, rfc, email1, email2);
			//valida
			
			if(Validator.isBlank(isValid)){
			
				System.out.println("username:::: " + username);
				System.out.println("rfc:::: " + rfc);
				System.out.println("email:::: " + email1);
				System.out.println("confirma email:::: " + email2);
				
				//User user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(), username);
									
					if(Validator.isNotNull(user)){

				        Boolean result = ConectorBaseDatos.actualizarCorreo(username, user.getEmailAddress(),email1);

				        if(result){
				        	
							user.setEmailAddress(email1);			        
					        
							UserLocalServiceUtil.updateUser(user);
				        	
							response.setRenderParameter("jspPage", "/html/emailupdater/ok.jsp");

				        }else{
				        	System.out.println("Ocurrio un error en el servicio ConectorBaseDatos.actualizarCorreo()");
				        }						
					}	
			}else{
				SessionMessages.add(request, 
						PortalUtil.getPortletId(request) 
						+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				
				
				SessionErrors.add(request, isValid);
			}
			
		
		} catch (PortalException e) {
			
		} catch (SystemException e) {
		
		}
		

		//guardar log de usuarios actualizados		
	}
	
	

	private String simple_validate(String username, String rfc, String email1, String email2){
		
		String rfc_correlacionador =  null;

		
		try{
			//error general en username
			if((Validator.isBlank(username)) || (Validator.isNull(username)) || (Validator.isHTML(username)) || (username.length() > 10)){
				//return "error-username";
				return "error-datos-incorrectos";
			}else{
				
				
				Criterion criterion = PropertyFactoryUtil.forName("screenName").eq(username);
				criterion = RestrictionsFactoryUtil.and(criterion , RestrictionsFactoryUtil.eq("companyId", company.getCompanyId()));

				
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(User.class).add(criterion);
				
					//error en username, no existe
					if(UserLocalServiceUtil.dynamicQuery(dynamicQuery).isEmpty()){
						return "error-username-not-exists";
					}else{
						user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(), username);
						
						//error en username, se encuentra inactivo
						if(user.getStatus() != 0){
							return "error-inactive-username";
						}else{
							rfc_correlacionador = CorrelacionadorWSClient.obtenerRFC(username);							
						}	
					}
			}
	
			
	
			//error general en rfc
			if((Validator.isBlank(rfc)) || (Validator.isNull(rfc)) ||(!Validator.isAlphanumericName(rfc)) || (rfc.length() > 20) || (!rfc.equalsIgnoreCase(rfc_correlacionador))){
				//return "error-rfc";
				return "error-datos-incorrectos";
			}
			
			
			//error general en email
			if((Validator.isBlank(email1)) || (Validator.isBlank(email2)) || (!Validator.isEmailAddress(email1)) || (!email1.equalsIgnoreCase(email2))){
				//return "error-email";
				return "error-datos-incorrectos";
			}else{
				Criterion criterion = PropertyFactoryUtil.forName("emailAddress").eq(email1);
				criterion = RestrictionsFactoryUtil.and(criterion , RestrictionsFactoryUtil.eq("companyId", company.getCompanyId()));
				
				
				
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(User.class).add(criterion);
				
				//error en email, ya existe
				if(!UserLocalServiceUtil.dynamicQuery(dynamicQuery).isEmpty()){
					
					List<User> usersList = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
					
					for(User user:usersList){
						

						System.out.println("emailAddress : " + user.getEmailAddress());

						System.out.println("status : " + user.getStatus());

						System.out.println("fullName : " + user.getFullName());

						System.out.println("companyId : " + user.getCompanyId());
						
					}
					
					return "error-email-exists";
				}
				
			}
		
		
		}catch(SystemException e){
		}catch (PortalException e) {
		}
		
		
		
		return "";
	}
	
	
	
	
	private String old_simple_validate(String username, String rfc, String email1, String email2){
		
		
		User user =  null;
		if((Validator.isBlank(username)) || (Validator.isNull(username)) || (Validator.isHTML(username)) || (username.length() > 10)){
			return "error-username";
		}else{
			

			/****************/
			
			try {
				user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(), username);
			} catch (PortalException e1) {
			} catch (SystemException e1) {
			}

			/******************/

			
			Criterion criterion = PropertyFactoryUtil.forName("emailAddress").eq(user.getEmailAddress());
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(User.class).add(criterion);
					
			try {
				if(!UserLocalServiceUtil.dynamicQuery(dynamicQuery).isEmpty()){
					return "error-email-exists";
				}
			} catch (SystemException e) {
			}
			
		}
		
		/*
		else if((Validator.isBlank(rfc)) || (Validator.isNull(rfc)) ||(!Validator.isAlphanumericName(rfc)) || (rfc.length() > 20)){
			return false;
		}*/
		
		

		
		
		
		
		if((Validator.isBlank(email1)) ||(!Validator.isEmailAddress(email1)) || (!email1.equalsIgnoreCase(email2))){
			/*
			try {
				
				
				
				Criterion criterion = PropertyFactoryUtil.forName("emailAddress").eq(email1);

				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(User.class).add(criterion);

				
				if(!UserLocalServiceUtil.dynamicQuery(dynamicQuery).isEmpty()){
					return "error-email";
				}
				
				//criterion = RestrictionsFactoryUtil.and(criterion , RestrictionsFactoryUtil.not(RestrictionsFactoryUtil.eq("fechaSiguienteSeguimiento", date)));
				//criterion = RestrictionsFactoryUtil.and(criterion , RestrictionsFactoryUtil.eq("estatus", "abierto"));
				
				
			} catch (SystemException e) {
				
			}
			*/
			
			return "error-email";
		}
		
		
		
		
		return "";
	}
	
	

	private boolean validate(String username, String rfc, String email1, String email2){
		
		String rfc_correlacionador =  null;
		
		
		if((Validator.isBlank(username)) || (Validator.isNull(username)) || (Validator.isHTML(username)) || (username.length() > 10)){
			return false;
		}else{
			rfc_correlacionador = CorrelacionadorWSClient.obtenerRFC(username);
			
			if(Validator.isBlank(rfc_correlacionador)){
				return false;
			}
		}
		
		
		if(!Validator.isBlank(rfc_correlacionador)){
			if((Validator.isBlank(rfc)) || (Validator.isNull(rfc)) ||(!Validator.isAlphanumericName(rfc)) || (rfc.length() > 20) || (rfc.equalsIgnoreCase(rfc_correlacionador))){
				return false;
			}
		}else{
			return false;
		}
		

		if((!Validator.isEmailAddress(email1)) || (!email1.equalsIgnoreCase(email2))){
			try {
				if(Validator.isNotNull(UserLocalServiceUtil.getUserByEmailAddress(company.getCompanyId(), email1))){
					return false;
				}
				
				
			} catch (PortalException e) {
				
			} catch (SystemException e) {
				
			}
			
			
			return false;
		}
		
		
		
		
		return true;
	}

	

    
}
