package com.dedge.global.custom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by kelceyguillenbejarano on 10/07/16.
 */
public class ConectorBaseDatos {

    public static Boolean actualizarCorreo(String idProveedor,String correoAntiguo, String correoNuevo){
        Connection conn  = null;
        try
        {
            String databaseURL = "jdbc:oracle:thin:@172.16.8.227:15210/EMIREPBD";
            String user = "RECEPPR_USER";
            String password = "RECEPPR_USER14#";
                Class.forName("oracle.jdbc.driver.OracleDriver");
             conn  = DriverManager.getConnection(databaseURL, user, password);
            // create our java preparedstatement using a sql update query
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE USUARIO_FISCAL SET EMAIL = ?, MODIFICADO = 'Y' WHERE ID_USUARIO = ? AND EMAIL = ?");

            // set the preparedstatement parameters
            ps.setString(1,correoNuevo);
            ps.setString(2,idProveedor);
            ps.setString(3, correoAntiguo);

            // call executeUpdate to execute our sql update statement
            int result = ps.executeUpdate();
            ps.close();
            return result>0;
        }catch (ClassNotFoundException ex) {
            System.out.println("Could not find database driver class");
            ex.printStackTrace();
        }
        catch (SQLException se)
        {
            // log the exception
            se.printStackTrace();
        }finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }
}
