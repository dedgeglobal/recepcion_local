package com.dedge.global.custom;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by kelceyguillenbejarano on 10/07/16.
 */
public class CorrelacionadorWSClient {

    public static String obtenerRFC(String numeroProveedor){

        try {
            URL oURL = new URL("http://172.18.227.208:80/Correlacionador/WsCorrelacionador");
            HttpURLConnection con = (HttpURLConnection) oURL.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction",
                    "");


            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:core=\"http://core.correlacionador.dedge.global/\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <core:obtenerDatos>\n" +
                    "         <paramsEntrada>{'VENDOR_ID':'" + numeroProveedor + "'}</paramsEntrada>\n" +
                    "         <idAplicacionCliente>proveedores</idAplicacionCliente>\n" +
                    "      </core:obtenerDatos>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            con.setDoOutput(true);
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(xml.getBytes());

            InputStream resStream = con.getInputStream();
            StringBuilder sb=new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(resStream));
            String read;

            while((read=br.readLine()) != null) {
                //System.out.println(read);
                sb.append(read);
            }

            br.close();
            String result = sb.toString();

            int _index = result.indexOf("STD_ID_NUM");
            int _indexC1=result.indexOf("\"", _index + 11);
            int _indexC2=result.indexOf("\"",_indexC1+1);
            System.out.println("_index:"+_index+",_indexC1:"+_indexC1+",_indexC2:"+_indexC2);
            System.out.println("RFC:"+result.substring(_indexC1+1,_indexC2));
            //System.out.println(result);
            //return result.substring(_indexC1+1,_indexC2);
            
            String rfc= result.substring(_indexC1+1,_indexC2);
            System.out.println("RFC:"+rfc);
            if(rfc==null || rfc.equalsIgnoreCase("roles"))
                return "";
            return rfc;
            
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "";

    }
}
